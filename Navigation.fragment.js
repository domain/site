import { html } from '@small-web/kitten'
import Styles from './Navigation.fragment.css'

const pages = [
  {id: 'home', link: '/', title: 'Home'},
  {id: 'getting-started', link: '/getting-started', title: 'Getting Started'},
  {id: 'technical-details', link: '/technical-details', title: 'Technical Details'},
  {id: 'faqs', link: '/faqs', title: 'FAQs'},
]

export default ({pageId}) => {
  return html`
    <nav>
      <ul>
        ${pages.map(page => html`
          <if ${pageId === page.id}>
            <then>
              <page title='Kitten: ${page.title}'>
              <li class='selected'>${page.title}</li>
            </then>
            <else>
              <li><a href='${page.link}'>${page.title}</a></li>
            </else>
          </if>
        `)}
        <li><a href='https://small-tech.org/fund-us/' target='_blank'>Fund Us</a></li>
      </ul>
    </nav>
    <${Styles} />
  `
}
