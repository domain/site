import Header from './Header.fragment.js'
import Footer from './Footer.fragment.js'
import Styles from './Main.fragment.css'

export default ({ title, pageId, SLOT }) => {
  return kitten.html`
    <page css syntax-highlighter>
    <${Header} pageId=${pageId} />
    <main>
      ${SLOT}
    </main>
    <${Footer} />
    <${Styles} />
  `
}
