export default function Footer () {
  return kitten.html`
    <footer>
      <p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://codeberg.org/kitten/site">This web site</a> &copy; 2024-present by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://ar.al">Aral Balkan</a> is licensed under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-SA 4.0</p> 
      <markdown>
        [Domain](https://codeberg.org/domian/app) &copy; 2021-present by Aral Balkan is free software licensed under [AGPL version 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html).

        Domain is brought to you with &hearts; by [Small Technology Foundation](https://small-tech.org).
    
        __[Like this? Fund us.](https://small-tech.org/fund-us)__ | [View source](https://codeberg.org/domain/site)
      </markdown>
      <style>
        footer { text-align: center; }
      </style>
    </footer>
  `
}
