import { html } from '@small-web/kitten'
import MainLayout from '../Main.layout.js'

export default () => html`
  <${MainLayout} pageId=getting-started>
    <h2>Getting Started</h2>
    <markdown>
        1. Sign up for accounts with the [supported service providers](#supported-service-providers).

        2. If you are going to host a public Small Web host open to the general public, also make sure that your domain is registered in the [Public Suffix List](https://publicsuffix.org/).

        3. Set up your own instance of Domain in seconds using Small Technology Foundation’s instance of Domain at [small-web.org](https://small-web.org) or learn the [technical details](/technical-details) of how to deploy Domain to your own server.

        ## Supported service providers

        Domain provides support for the following service providers. You will need to get accounts with them to configure it.

        ### DNS

        - [DNSimple](https://dnsimple.com)

        ### VPS

        - [Hetzner Cloud](https://www.hetzner.com/cloud)

        The VPS setup is handled via cloud-init. To debug any issues you might experience with the server setup, the following commands (executed on the server) should help:

        [code shell]
        cloud-init analyze dump
        systemctl status cloud-final.service
        cat /var/log/cloud-init-output.log
        [code]

        ### Payment

        - __None:__ for private instances (e.g., families, internal use at organisations, etc.)

        - __Tokens:__ for use in place of money by organisations running public instances (e.g., a city providing free Small Web Places to its citizens). For more information, see [What is the token payment type?](/faqs/#what-is-the-token-payment-type) __(Under construction.)__

        - __[TBD]():__ for commercial payments (e.g., for making your Small Web Domain financially sustainable within the capitalist system you find yourself living under) __(Under construction.)__

    </markdown>
  </>
`
