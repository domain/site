import { html } from '@small-web/kitten'
import MainLayout from '../Main.layout.js'

export default () => html`
  <${MainLayout} pageId=faqs>
    <h2>Frequently-Asked Questions (FAQs)</h2>
    <markdown>
      ## How can I support development of Domain, Kitten, and the Small Web?

      Domain, Kitten, and the Small Web are being built by Small Technology Foundation, a tiny, two-person not-for-profit based in Ireland.

      We don’t take venture capital or money from surveillance capitalists. We’re funded by inviduals like you.

      You can [support us by becoming a patron or making a one-time donation](https://small-tech.org/fund-us).

      Thank-you for helping us continue to exist.

      ## Is Domain ready for use?

      No. It is under heavy development with an expected initial release in 2024.

      ## Why such a generic name?

      On purpose. We are not trying to create A Brand™ here. The “brand”, if anything, is the instantly-recognisable simplicity and typographic treatment of the public-facing sign-up page itself which will enable people to recognise Small Web domains (due to the lack of bullshit/noise). Otherwise, the focus is on the domain itself, as it should be.

      ## What does it take to run a Small Web Domain?

      Domain integrates three* main services:

      1. A Domain Name Service (DNS) provider for setting up domains.

      2. A Virtual Private Server (VPS) provider for settings up servers.

      3. A payment provider to limit access to scarce resources.

      _\* For public instances, it also requires that your domain is registered on the [Public Suffix List](https://publicsuffix.org/)._

      ## What is the token payment type?

      The Small Web aims to be a bridge between the capitalist centralised web and a post-capitalist humanscale web of individually-owned-and-controlled single-tenant web places. As such, we have the unenviable task of trying to design a system that is both sustainable under capitalism and viable for post-capitalist use.

      Supporting both Stripe and tokens as a payment type is an example of this.

      A token is simply a secret code that you can enter in place of traditional payment with money.

      For example, a municipality might decide that its citizens having their own place on the Small Web is good for human rights and democracy and might budget to provide them with this service from the common purse, for the common good. As such, it might create codes that get mailed out to all citizens. They can then use these codes in place of payment. (We prototyped an early version of this with the City of Ghent several years ago. Unfortunately, a conservative government came into power and our funding for the project was cut off.)

      Traditional/token payment doesn’t have to be mutually exclusive. The municipality in question might, for example, enable both so that people can sign up for more than one Small Web place or if it wants to enable others (e.g., people who are not residents of their city) to sign up.

    </markdown>
  </>
`
