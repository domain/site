import kitten from '@small-web/kitten'
import Navigation from './Navigation.fragment.js'

export default function ({pageId}) {
  return kitten.html`
    <header>
      <img
        id='logo'
        src='/images/logo.svg'
        alt='Domain logo: a minimalist illustation of an adorable kitten’s head with pink nose and ears.'
      >
      <h1>Domain</h1>
      <${Navigation} pageId=${pageId}/>
    </header>
  `
}
