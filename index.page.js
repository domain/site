import kitten from '@small-web/kitten'

import MainLayout from './Main.layout.js'

export default ({ request }) => {
  return kitten.html`
    <${MainLayout} pageId='home'>
      <div class='subtitle'>
        <markdown>
          💕 [Small Web](https://ar.al/2020/08/07/what-is-the-small-web/) homes for your [Kittens](https://codeberg.org/kitten/app).
        </markdown>
      </div>
      <markdown>
        Domain lets you become a [Small Web](https://ar.al/2020/08/07/what-is-the-small-web/) host.

        Think of it as a batteries-included free and open Digital Ocean in-a-box for [Kitten](https://codeberg.org/kitten/app)-based apps.

        ## For organisations to serve individuals

        Organisations can use Domain to provide Small Web hosting services at their own Small Web domains to their communities.

        These services can be:

        - A commercial activity (e.g., our not-for-profit Small Technology Foundation is planning on charging ~€10/month for small web places on small-web.org in order to fund ongoing development of Kitten, Domain, and our other work.)

        - For the public good (e.g., a municipality could provide free Small Web places to its citizens via tokens. ([Small Technology Foundation](https://small-tech.org) actually prototyped such a system with the City of Ghent a few years ago.)

        - A *hyperlocal* or more personal initiative (e.g., providing Small Web places for your neighbourhood or your own family via a private instance).

        Individuals can use Small Web hosts to set up their own Small Web places at Small Web domains without any technical knowledge.

        ## Small Web Domain

        A Small Web domain is, for all intents and purposes, like a top-level domain but:

        1. It is a second-level domain (e.g., small-web.org),

        2. Where Small Web places are registered as subdomains,

        3. And, unless it is being run as a private instance (e.g., for your family), where the second-level domain is registered on the [Public Suffix List](https://publicsuffix.org/).

        If a person wants to move their place to a different Small Web domain or to a Big Web domain (i.e., their own second-level domain), they should be able to do so at any time.

        ## Apps

        A Small Web domain enables you to install apps written for [Kitten](https://codeberg.org/kitten/app).

        By limiting to scope to apps written in Kitten we enable the following properties:

        - **Generation of a secret** that only the person who owns the site being commissioned knows, which they can use both within the app they’re installing and to administer their hosting relationship with the Domain instance (e.g., to cancel their hosting, etc.)

        - **Integrated domain administration functionality** in the Small Web app via server-to-server communication between the Small Web host and the person’s Small Web place.

        ## Like this? Fund us!

        [Small Technology Foundation](https://small-tech.org) is a tiny, independent not-for-profit.

        We exist in part thanks to patronage by people like you. If you share [our vision](https://small-tech.org/about/#small-technology) and want to support our work, please [become a patron or donate to us](https://small-tech.org/fund-us) today and help us continue to exist.
      </markdown>
    </>
  `
}
