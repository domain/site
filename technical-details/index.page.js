import { html } from '@small-web/kitten'
import MainLayout from '../Main.layout.js'

export default () => html`
  <${MainLayout} pageId=technical-details>
    <markdown>
      ## Technical Details

      ### Deploy

      To deploy Domain to a production server and have it run as a systemd daemon:

      1. Install [Kitten](https://kitten.small-web.org).
      2. Run:

          [code shell]
            kitten deploy https://codeberg.org/domain/app.git
          [code]

      > 💡 If you don’t want to set up your own server and deploy Domain manually, you can also use our Domain instance to set up your own in seconds at [small-web.org](https://small-web.org).

      ### Install (development)

      On your development machine:

      1. Install [Kitten](https://kitten.small-web.org).
      2. Clone this repo.

      ### Run (development)

      Domain is a [Kitten](https://codeberg.org/kitten/app) app. There is no build stage.

      From the Domain folder, simply run:

      [code shell]
      kitten
      [code]

      - Main page: https://localhost
      - Settings Panel: https://localhost/settings

      For the Settings Panel password please see terminal output on first run. Please store this secret in your password manager.

      > 💡 Note that when running in development, you cannot use Domain to create new Small Web places when running at _localhost_. Instead, you must use expose your development machine at a publicly-reachable domain so that the servers you’re setting up have an address to communicate with.

      ### Configure

      Once you sign up for accounts with the [supported service providers](/getting-started/#supported-service-providers) and Domain is running, you can configure your account using the Admin Panel.

      ### Troubleshooting VPS

      The VPS setup is handled via cloud-init. To debug any issues you might experience with the server setup, the following commands (executed on the server) should help:

      [code shell]
      cloud-init analyze dump
      systemctl status cloud-final.service
      cat /var/log/cloud-init-output.log
      [code]

      ### Cryptographical properties

      Please see the [cryptographical properties section in the Kitten documentation](https://codeberg.org/kitten/app#user-content-cryptographical-properties).
    </markdown>
  </>
`
